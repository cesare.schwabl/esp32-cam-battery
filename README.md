# ESP32-CAM-Battery Board V1.0

## Design Parameters
+ CP2102 USB > UART
+ 3.3V LDO
+ LiPo Charger
+ On/Off Switch
+ same size as ESP32-CAM Module


![Draft](ESP32-CAM-Battery.png)